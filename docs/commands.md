```shell script
yarn add artillery
yarn quick # artillery quick --count 10 -n 20 https://www.urjc.es/
```

Output:

```shell
➜  load_tests git:(master) ✗ yarn quick        
yarn run v1.22.4
$ artillery quick --count 10 -n 20 https://www.urjc.es/
Started phase 0, duration: 1s @ 16:49:27(+0100) 2021-01-30
Report @ 16:49:31(+0100) 2021-01-30
Elapsed time: 4 seconds
  Scenarios launched:  10
  Scenarios completed: 10
  Requests completed:  200
  Mean response/sec: 51.81
  Response time (msec):
    min: 22.6
    max: 307.7
    median: 44.4
    p95: 138.1
    p99: 229.4
  Codes:
    200: 200

All virtual users finished
Summary report @ 16:49:31(+0100) 2021-01-30
  Scenarios launched:  10
  Scenarios completed: 10
  Requests completed:  200
  Mean response/sec: 51.68
  Response time (msec):
    min: 22.6
    max: 307.7
    median: 44.4
    p95: 138.1
    p99: 229.4
  Scenario counts:
    0: 10 (100%)
  Codes:
    200: 200

✨  Done in 5.28s.
```

Ahora modificamos el script con nuestra aplicación levantada y lo volvemos a ejecutar sobre `https://localhost:8443/api/books/`

```shell
➜  load_tests git:(master) ✗ yarn quick
yarn run v1.22.4
$ artillery quick --count 10 -n 20 -k https://localhost:8443/api/books/
Started phase 0, duration: 1s @ 17:00:58(+0100) 2021-01-30
Report @ 17:00:59(+0100) 2021-01-30
Elapsed time: 1 second
  Scenarios launched:  10
  Scenarios completed: 10
  Requests completed:  200
  Mean response/sec: 147.06
  Response time (msec):
    min: 2
    max: 35.5
    median: 3.5
    p95: 14.6
    p99: 30.8
  Codes:
    200: 200

All virtual users finished
Summary report @ 17:00:59(+0100) 2021-01-30
  Scenarios launched:  10
  Scenarios completed: 10
  Requests completed:  200
  Mean response/sec: 145.99
  Response time (msec):
    min: 2
    max: 35.5
    median: 3.5
    p95: 14.6
    p99: 30.8
  Scenario counts:
    0: 10 (100%)
  Codes:
    200: 200

✨  Done in 2.52s.
```
