# Test de carga con artillery

En este proyecto se encuentra la solución para la ejecución de test de carga sobre una
aplicación en Spring Boot.

## Configuración

Lo primero que tenemos que hacer es instalar las dependencias del proyecto:

```shell
npm install
# O
yarn install
```

Lo siguiente es ir al directorio en el que está nuestra aplicación de libros en Spring y arrancarla:

```shell
mvn clean install
java -jar target/*.jar
```

## Ejecución de los tests de carga

Para lanzar los tests de carga, simplemente es necesario lanzar el comando start:

```shell
npm start
# O
yarn start
```

En el directorio `outputs` encontraremos el fichero `output.json` con el resultado de la prueba.
